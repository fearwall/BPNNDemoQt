#include "inc/mainwindow.h"
#include "inc/classifiercontroller.h"
#include <QApplication>

int main(int argc, char *argv[])
{
#ifdef Q_OS_WINDOWS
    // Windows系统启用高DPI设置
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
